## BACKEND RELATED TASKS__________________________________________________________

# Define env var as shorthand.
WORDPRESS_TOOLBOX=docker-compose run --rm wp_tools

# Start the container stack via docker-compose.
# Rebuild the stack everytime a Dockerfile changed.
wp_start:
	@echo "==> Starting backend containers..."
	docker-compose up --build

# Just stop the containers.
wp_stop:
	@echo "==> Stopping backend containers..."
	docker-compose stop

# Run a complete installation with the toolbox / wp-cli container.
install: wp_start # chmod_wordpress_install
	$(WORDPRESS_TOOLBOX) install

# Workaround for permission problems.
# FIXED: Remove, find better options via shared volumes.
#chmod__install:
#	sudo chmod -R 777 ./wordpress
#	sudo cp -r ./headless-theme ./wordpress/wp-content/themes/.

# Configure the wordpress installation via wp-cli container.
configure:
	$(WORDPRESS_TOOLBOX) configure

# Stop and remove containers and associated material.
clean: wp_stop
	@echo "==> Removing Wordpress traces..."
	@sudo rm -rf ./wordpress/
	@echo "==> Removing Docker containers..."
	docker-compose down

# Backup the database to an sql file. This overwrites existing files.
.PHONY: backup # Ensure this is always executed (like --force)
backup:
	rm -rf backup/
	mkdir backup && cd backup
	docker exec wp_db sh -c 'exec mysqldump --databases wordpress -uroot -p"wordpressdbrootpasswd"' > backup/wordpress.sql

# Get a backup file from backup/wordpress.sql and apply it to the MySQL instance
upback:
	@docker exec -i wp_db sh -c 'exec mysql -uroot -p"wordpressdbrootpasswd"' < backup/wordpress.sql

# Purge the Database Persistence AND Volumes if desired.
purge: clean
	docker-compose down --volumes

# Copy headless theme on change to wp volume
copy_theme:
	docker-compose up --build wp_copytheme

# Server Related Tasks
registry:
	docker-compose build
	docker-compose push

deploy:
	docker-compose -f "docker-compose-prod.yml" up --build -d


backup_server:
	rm -rf backup/
	mkdir backup && cd backup
	docker exec wp_db sh -c 'exec mysqldump --databases wordpress -uroot -p"vkqrfXrDdqVZ89Q68S3YgK9ghKBH9zt7AwcmnKa7XXrS25N4VVUBQXXQxaYC5y6t"' > backup-server/wordpress.sql

upback_server:
	@docker exec -i wp_db sh -c 'exec mysql -uroot -p"vkqrfXrDdqVZ89Q68S3YgK9ghKBH9zt7AwcmnKa7XXrS25N4VVUBQXXQxaYC5y6t"' < backup-server/wordpress.sql

## FRONTEND RELATED TASKS________________________________________________________
# Because of Headless-CMS-Architecture and for development purposes (logging output
# , seperate processes) frontend tasks are not handeled via this Makefile.

## COMMON TASKS__________________________________________________________________

start: wp_start

stop: wp_stop
