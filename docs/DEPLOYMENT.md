# Using docker-compose and --host flag for deployment

	docker-compose --host "ssh://archie@aizr.one" up --build
	

This may not work because a number of reasons:

- full repository needs to be on server machine
- separate `docker-compose.yml` and possibly `Makefile` with env variables for server needed


# Deploy using docker swarm

## Preperation

For now: Build images locally with:
 
    docker-compose build
    
Login (one-time per machine) and deploy to Gitlab container registry. 
 
    docker login registry.gitlab.com
    
    docker-compose push
    
Note: Images need to be tagged via key `image` in  `docker-compose.yml` to work with Gitlab Container Registry:

    image: registry.gitlab.com/aizr1/bach/wp_db

## Run single node swarm locally

    docker swarm init
    
    docker stack deploy -c "docker-compose.yml" bach
    
Problems/Notes:

- Containers start, but do not talk in networks. Maybe change network settings in  `docker-compose.yml`
- `wp_php` not talking to `wp_http` because of `nginx.conf' via port 9000 
- nginx.conf is not copied to to docker stack deploy
- Workaround: COPY nginx.conf via Dockerfile not via bind
- Find original wordpress nginx config https://www.nginx.com/resources/wiki/start/topics/recipes/wordpress/
- Resolving works with correct nginx.conf after some trail and error

    
## Run single node swarm remotely 

Problems/Notes: 

- Containers do not talk to each other  https://success.docker.com/article/error-network-sandbox-join-failed-during-service-restarts
- some network device cannot be created
- maybe the nginx config needs to be revised.
- wp cli (toolbox) generated contents completly missing in fresh `stack deploy`
- docker-compose / make unuseable for cd chain, because multiple containers are involved in building a volume, volume consistency not easy to implement
- find a docker stack setup that used "finished" or "sealed" containers not depending on other "build" containers
- a shared volume is a native directory on the machine host, is has to be created by something else than `stack deploy` e.g. a build script or `docker-compose up`

## Differences `docker-compose up` and `docker stack deploy`

_Source: https://vsupalov.com/difference-docker-compose-and-docker-stack/_

### `docker stack deploy`

- ignores `build` instruction
- needs prebuilt images to exist in a registry `image: registry.gitlab.com/aizr1/bach/wp_db`
- parts of compose file are ignored: e.g. `container_name`, `build`, `restart`, `depends_on` etc.
- included with docker engine, written in go
- a docker swarm (container orchestration) needs to be created, in which a stack lives
- needs `Compose file specification 3` 
- support `deploy` key (replicas, restart_policy, ...)

### `docker-compsoe`

- is a python project, acquired by docker
- creates stacks of containers
- uses docker api to bring up containers according to a specification
- installed separately from core docker as `docker-compose` package
- can handle `Compose file specification 2` and `3` 

### Conclusion
- both `docker stack deploy` and `docker-compsoe` use `docker-compose.yml` file `v3`
- both do almost the same, `docker stack deploy` is newer
- use depends on factors like storage, legacy migration, app dependencies, ...

## My top reasons for a single-node Swarm over docker-compose:

_Source: https://devopstuto-docker.readthedocs.io/en/latest/docker_swarm/articles/2018/01__2018_10_17/01__2018_10_17.html_

- It only takes a single command to create a Swarm from that docker host docker swarm init.

- It saves you from needing to manually install/update docker-compose on that server. Docker engine is installable and updatable via common Linux package managers (apt, yum) via https://store.docker.com but docker-compose is not.

- When you’re ready to become highly-available, you won’t need to start from scratch. Just add two more nodes to a well-connected network with the 1st node. Ensure firewall ports are open between them. Then use docker swarm join-token manager on 1st node and run that output on 2nd/3rd. Now you have a fully redundant raft log and managers. Then you can change your compose file for multiple replicas of each of your services and re-apply with docker stack deploy again and you’re playin’ with the big dogs!

- You get a lot of extra features out-of-the-box with Swarm, including secrets, configs, auto-recovery of services, rollbacks, healtchecks, and ability to use Docker Cloud Swarms BYOS to easily connect to swarm without SSH.

- Healthchecks, healthchecks, healthchecks. docker run and docker-compose won’t re-create containers that failed a built-in healthcheck. You only get that with Swarm, and it should always be used for production on all containers.

- Rolling updates. Swarm’s docker service update command (which is also used by docker stack deploy when updating yaml changes) has TONS of options for controlling how you replace containers during an update. If you’re running your own code on a Swarm, updates will be often, so you want to make sure the process is smooth, depends on healthchecks for being “ready”, maybe starts a new one first before turning off old container, and rolls back if there’s a problem. None of that happens without Swarm’s orchestration and scheduling.

- Local docker-compose for development works great in the workflow of getting those yaml files into production Swarm servers.

- Docker and Swarm are the same daemon, so no need to worry about version compatibility of production tools. Swarm isn’t going to suddenly make your single production server more complex to manage and maintain.


# Deployment Strategy

------

## Using only `docker-compose`


- clone repository
- use customized `docker-compose.yml` for server environment
- use customized configs if needed
- build images and run stack with docker-compose
- dco ensures startup order, create volumes
- wp_toolbox configures main containers
- services easy to manage and maintain with `docker-compose` (e.g. backups, wp upgrades, plugin updates via wp_toolbox/wp_cli)
- profit


### Benefits

- simplicity
- easy setup via `make`
- simple to maintain structure
- container names are very clear
- general benefits of using docker on a machine
- suitable for monolitic setups, convenience in managing a single machine

------
## Using `docker-compose` and `docker stack deploy`

- clone repository
- fill volumes / load configs
- build images
- push images to a registry
- deploy to swarm

### Benefits

- updating main containers on the fly
- create replicas 
- multi-node environment possible
- load-balancing possible

### Limitations
- will only run on single node swarm (shared storage, database)
- needs docker-compose for building and managing containers
- files, volumes created by `wp_tools` needs to be spread to nodes
- database? > cloud options, distributed databases to be evaluated
- other changes needed to be multi-node compatible

------

## Future: Clean `docker stack deploy`

Source: https://blog.rimuhosting.com/2019/05/09/getting-started-with-docker-stacks/
Source: https://www.scalewp.io/

- local: clone repo
- local: build and push images to registry
- manager: retrieve images and deploy the stack

or more CD:

- local: change backend related code and push to repo
- cd: checkout sources, pack files, build image, push image to registry (for changed images only)
- cd: deploy stack
- stack: update only affected nodes

### Benefits

- full benefits of swarm and stacks
- usable in highly scaled environments > high avaibility, cdn-like

### Limitations

- shared volumes needed if scaled
- database scaling is complex topic

# CI/CD with GitLab

TODO:

- upfront disable rss feed
- upfront disable xml-rpc

- create ssh machine user
- create gitlab yml file
- define pipeline "deploy"
- define "build" job
- define "deploy 2 machine" job







