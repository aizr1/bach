# Building a docker stack

Targets:

- Easy to use in local development
- Simple to reproduce setup
- Same Surroundings of local development setup can be used in production
- CI/CD compatible e.g. for automated deployment

# Reading List:
http://blog.michaelperrin.fr/2018/10/26/automate-wordpress-part-1/

https://vsupalov.com/database-in-docker/

https://docs.docker.com/compose/compose-file

___________ OLD NOTES_____________________

# custom wordpress image

sources to pack into custom wordpress image

docker hub => base wordpress image 	=> custom image
gitlab => main/child theme 		=> custom image
gitlab => plugins 			=> custom image
wordpress.org => plugins 	    	=> WPackagist => composer => custom image

# Steps needed to build custom image:

## Dockerfile

Prepare a single custom image with all properties needed. In this case: Wordpress with all dependencies like plugins, themes, and surrounding extras.

_Proper prio to be found._

- use base image from docker hub: wordpress
- use more refined base image: https://github.com/yobasystems/alpine-php-wordpress
- for ssl support install certs
- apk update
- apk add composer
- add a theme from a git source (eg. frontend in vue or react?!)
- add any custom plugins from git source
- use wpackagist mirrored plugin repo with composer.json
- install external plugins via composer/wpackagist
- alternative use wp-cli in some way to install custom plugins (must use ssh, maybe harder to config)
- nfs mount /uploads to a specific place like /usr/src/wordpress/wp-content/uploads to persist user uploads on a bare metal machine


## docker-compose.yml

_Combine custom images with base images to a stack._

- pull custom image from gitlab repository
- pull mariadb
- mount volumes
- build a network between containers
- expose a port 80 some where


## .gitlab-ci.yml

_Use stack and deploy it to some kind of swarm cluster_

- deploy a docker swarm to some dedicated swarm hoster (physical machine)

