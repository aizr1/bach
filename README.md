```
    ____  ___   ________  __________    ____  ____ 
   / __ )/   | / ____/ / / / ____/ /   / __ \/ __ \
  / __  / /| |/ /   / /_/ / __/ / /   / / / / /_/ /
 / /_/ / ___ / /___/ __  / /___/ /___/ /_/ / _, _/ 
/_____/_/  |_\____/_/ /_/_____/_____/\____/_/ |_|  
```

Bachelor Project


# About

Is it possible to build a web application as frontend for a WordPress installation?


# First time installation

We need these packages...

    docker
    docker-compose
    make
    node
    yarn
    
...installed on any machine where the stack is supposed to run.
    
We are using...

    make 

...to install this _backend_ docker stack more convenient.

**Note**: If something fails, just re-run `make`. Often the `mysql` container is slow, when its initialized for the first time.

After the installation is complete the backend stack will run in verbose mode, so we can see whats happening under the hood.

Open a new terminal session for the frontend installation step.

For the **Frontend** we need to install some npm packages by running.

    cd nuxt && yarn install
    
After the installation is complete, the frontend development server can be started by running

    yarn dev
    
Now we have to sessions with logging and error output for the backend and the frontend.
See below on how to stop and maintain the processes.

## ReInstall / Configure Wordpress:

_Note: Will also configure the installation._

    make install

Configure Wordpress (when the config has changed):

    make configure

Wordpress was **installed** and **configured** without any manual
clicks in some interface and is now ready to be visited at http://localhost .

# Usage

## Start / Stop the **Backend** Stack

Warning: It is recommended to stop the backend stack _gently_ if database contents should be reused. You can do so, by using this command:

    make stop
    
    make start
    
    Admin: http://localhost/wp-admin
    OEmbed API: http://localhost/wp-json
    
## Start / Stop the **Frontend** Stack

Note: Frontend is fully decoupled from Backend. But in order to communicate with the frontend correctly, backend stack needs to be run first. ;)

    cd nuxt && yarn dev
    
Kill the terminal process to stop the frontend server.
  
# Maintenance

**Note: For remote maintenance the same commands can be run on a remote machine via `ssh`.(If the this repo exists on a remote machine)**

## Changed `headless-theme` files?

Because of file permission problems with volumes, these files need to be manually copied on change for now.

    make copy_theme


## Using full wp-cli via wp_tools container

Create an alias for easier usage:

    alias wp="docker-compose run --entrypoint "wp" --rm wp_tools"

basic pattern:

    docker-compose run --entrypoint "wp" --rm wp_tools <your-wpcli-commands-here>
    
usage with alias:

    wp
    
    wp post list
    
Documentation: https://developer.wordpress.org/cli/commands/
    
    
## Make a DB Backup / Reinstall SQL Database

    make backup
    
    make upback
    
## Update Wordpress

Change the Wordpress version in the `WORDPRESS_VERSION` environment variable
in `docker-compose.yml` and type this command:

    make update
    
## Remove everything from current machine (excluding volumes)

    make clean
    
## Remove everything from current machine (INCLUDING volumes)

Warning: Will remove complete installation and all data.

    make purge
    
Note: sqeaky clean surroundings can be achieved by pruning containers/images, volumes.

    docker [volume, container or volume] prune


# Deployment

Gitlab CI/CD is used for full automation of the deployment process to a server. Its configured in the `.gitlab-ci.yml` file.

1. alpine container with is launched, install openssh

2. connection to target machine is initialized

3. repo is cloned

4. switch to current branch

5. build the docker stack and up it

6. remove repo and build traces for security

7. run a healthcheck to verify that the backend is reachable





                                                   
