<?php

/*
GLOBAL REST API SWITCH
*/

//add_filter('rest_enabled', '_return_false');
//add_filter('rest_jsonp_enabled', '_return_false');

/*
 * MAIN ENDPOINT FILTER BEGIN
 *
 * Every REST Endpoint can be toggled here. The $endpoints array is modified and returned.
 * Some REST Routes should not be exposed to the public, because they could be used to modify
 * data or change settings.
 */

add_filter('rest_endpoints', function ($endpoints) {

    // DISABLE USERS
    // https://developer.wordpress.org/rest-api/reference/users/

//    if ( isset( $endpoints['/wp/v2/users'] ) ) {
//        unset( $endpoints['/wp/v2/users'] );
//    }

//    if ( isset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] ) ) {
//        unset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] );
//    }
//

//    if ( isset( $endpoints['/wp/v2/users/me'] ) ) {
//        unset( $endpoints['/wp/v2/users/me'] );
//    }

    // DISABLE SETTINGS
    // https://developer.wordpress.org/rest-api/reference/settings/

    if (isset($endpoints['/wp/v2/settings'])) {
        unset($endpoints['/wp/v2/settings']);
    }

    // DISABLE COMMENTS
    // https://developer.wordpress.org/rest-api/reference/comments/

//    if ( isset( $endpoints['/wp/v2/comments'] ) ) {
//        unset( $endpoints['/wp/v2/comments'] );
//    }

//    if ( isset( $endpoints['/wp/v2/comments/(?P<id>[\d]+)'] ) ) {
//        unset( $endpoints['/wp/v2/comments/(?P<id>[\d]+)'] );
//    }

//    if ( isset( $endpoints['/wp/v2/media'] ) ) {
//        unset( $endpoints['/wp/v2/media'] );
//    }

//    if ( isset( $endpoints['/wp/v2/media/(?P<id>[\d]+)'] ) ) {
//        unset( $endpoints['/wp/v2/media/(?P<id>[\d]+)'] );
//    }

    // DISABLE PAGES
    // https://developer.wordpress.org/rest-api/reference/pages/

//    if ( isset( $endpoints['/wp/v2/pages'] ) ) {
//        unset( $endpoints['/wp/v2/pages'] );
//    }

//    if ( isset( $endpoints['/wp/v2/pages/(?P<id>[\d]+)'] ) ) {
//        unset( $endpoints['/wp/v2/pages/(?P<id>[\d]+)'] );
//    }

    // DISABLE TAGS
    // https://developer.wordpress.org/rest-api/reference/tags/

//
//    if ( isset( $endpoints['/wp/v2/tags'] ) ) {
//        unset( $endpoints['/wp/v2/tags'] );
//    }

//    if ( isset( $endpoints['/wp/v2/tags/(?P<id>[\d]+)'] ) ) {
//        unset( $endpoints['/wp/v2/tags/(?P<id>[\d]+)'] );
//    }

    // DISABLE CATEGORIES
    // https://developer.wordpress.org/rest-api/reference/categories/

//    if ( isset( $endpoints['/wp/v2/categories'] ) ) {
//        unset( $endpoints['/wp/v2/categories'] );
//    }
//
//    if ( isset( $endpoints['/wp/v2/categories/(?P<id>[\d]+)'] ) ) {
//        unset( $endpoints['/wp/v2/categories/(?P<id>[\d]+)'] );
//    }

    // DISABLE POST STATUSES
    // https://developer.wordpress.org/rest-api/reference/statuses/

//    if ( isset( $endpoints['/wp/v2/statuses'] ) ) {
//        unset( $endpoints['/wp/v2/statuses'] );
//    }
//
//    if ( isset( $endpoints['/wp/v2/statuses/(?P<status>[\w-]+)'] ) ) {
//        unset( $endpoints['/wp/v2/statuses/(?P<status>[\w-]+)'] );
//    }

    // DISABLE POST TYPES
    // https://developer.wordpress.org/rest-api/reference/types/

//    if (isset($endpoints['/wp/v2/types'])) {
//        unset($endpoints['/wp/v2/types']);
//    }
//
//    if (isset($endpoints['/wp/v2/types/(?P<type>[\w-]+)'])) {
//        unset($endpoints['/wp/v2/types/(?P<type>[\w-]+)']);
//    }

    // DISABLE TAXONOMIES
    // https://developer.wordpress.org/rest-api/reference/taxonomies/

    if (isset($endpoints['/wp/v2/taxonomies'])) {
        unset($endpoints['/wp/v2/taxonomies']);
    }

    if (isset($endpoints['/wp/v2/taxonomies/(?P<taxonomy>[\w-]+)'])) {
        unset($endpoints['/wp/v2/taxonomies/(?P<taxonomy>[\w-]+)']);
    }

    // KEEP POSTS
    // https://developer.wordpress.org/rest-api/reference/posts/

//    if ( isset( $endpoints['/wp/v2/posts'] ) ) {
//        unset( $endpoints['/wp/v2/posts'] );
//    }
//
//    if ( isset( $endpoints['/wp/v2/posts/(?P<id>[\d]+)'] ) ) {
//        unset( $endpoints['/wp/v2/posts/(?P<id>[\d]+)'] );
//    }

    // DISABLE POST REVISIONS

    if (isset($endpoints['/wp/v2/posts/(?P<parent>[\d]+)/revisions'])) {
        unset($endpoints['/wp/v2/posts/(?P<parent>[\d]+)/revisions']);
    }


    if (isset($endpoints['/wp/v2/posts/(?P<parent>[\d]+)/revisions/(?P<id>[\d]+)'])) {
        unset($endpoints['/wp/v2/posts/(?P<parent>[\d]+)/revisions/(?P<id>[\d]+)']);
    }

    // DISABLE PAGE REVISIONS

    if (isset($endpoints['/wp/v2/pages/(?P<parent>[\d]+)/revisions'])) {
        unset($endpoints['/wp/v2/pages/(?P<parent>[\d]+)/revisions']);
    }

    if (isset($endpoints['/wp/v2/pages/(?P<parent>[\d]+)/revisions/(?P<id>[\d]+)'])) {
        unset($endpoints['/wp/v2/pages/(?P<parent>[\d]+)/revisions/(?P<id>[\d]+)']);
    }

    // DISABLE ADDITIONAL ACF OUTPUT (FROM acf-to-rest PLUGIN)

    if (isset($endpoints['/acf/v3'])) {
        unset($endpoints['/acf/v3']);
    }

    if (isset($endpoints['/acf/v3/posts'])) {
        unset($endpoints['/acf/v3/posts']);
    }

    if (isset($endpoints['/acf/v3/posts/(?P<id>[\\d]+)/?(?P<field>[\\w\\-\\_]+)?'])) {
        unset($endpoints['/acf/v3/posts/(?P<id>[\\d]+)/?(?P<field>[\\w\\-\\_]+)?']);
    }

    if (isset($endpoints['/acf/v3/pages'])) {
        unset($endpoints['/acf/v3/pages']);
    }

    if (isset($endpoints['/acf/v3/pages/(?P<id>[\\d]+)/?(?P<field>[\\w\\-\\_]+)?'])) {
        unset($endpoints['/acf/v3/pages/(?P<id>[\\d]+)/?(?P<field>[\\w\\-\\_]+)?']);
    }


    if (isset($endpoints['/acf/v3/media'])) {
        unset($endpoints['/acf/v3/media']);
    }

    if (isset($endpoints['/acf/v3/media/(?P<id>[\\d]+)/?(?P<field>[\\w\\-\\_]+)?'])) {
        unset($endpoints['/acf/v3/media/(?P<id>[\\d]+)/?(?P<field>[\\w\\-\\_]+)?']);
    }

    if (isset($endpoints['/acf/v3/blocks'])) {
        unset($endpoints['/acf/v3/blocks']);
    }

    if (isset($endpoints['/acf/v3/blocks/(?P<id>[\\d]+)/?(?P<field>[\\w\\-\\_]+)?'])) {
        unset($endpoints['/acf/v3/blocks/(?P<id>[\\d]+)/?(?P<field>[\\w\\-\\_]+)?']);
    }

    if (isset($endpoints['/acf/v3/categories'])) {
        unset($endpoints['/acf/v3/categories']);
    }

    if (isset($endpoints['/acf/v3/categories/(?P<id>[\\d]+)/?(?P<field>[\\w\\-\\_]+)?'])) {
        unset($endpoints['/acf/v3/categories/(?P<id>[\\d]+)/?(?P<field>[\\w\\-\\_]+)?']);
    }

    if (isset($endpoints['/acf/v3/tags'])) {
        unset($endpoints['/acf/v3/tags']);
    }

    if (isset($endpoints['/acf/v3/tags/(?P<id>[\\d]+)/?(?P<field>[\\w\\-\\_]+)?'])) {
        unset($endpoints['/acf/v3/tags/(?P<id>[\\d]+)/?(?P<field>[\\w\\-\\_]+)?']);
    }

    if (isset($endpoints['/acf/v3/comments'])) {
        unset($endpoints['/acf/v3/comments']);
    }

    if (isset($endpoints['/acf/v3/comments/(?P<id>[\\d]+)/?(?P<field>[\\w\\-\\_]+)?'])) {
        unset($endpoints['/acf/v3/comments/(?P<id>[\\d]+)/?(?P<field>[\\w\\-\\_]+)?']);
    }

    if (isset($endpoints['/acf/v3/options/(?P<id>[\\w\\-\\_]+)/?(?P<field>[\\w\\-\\_]+)?'])) {
        unset($endpoints['/acf/v3/options/(?P<id>[\\w\\-\\_]+)/?(?P<field>[\\w\\-\\_]+)?']);
    }

    if (isset($endpoints['/acf/v3/users'])) {
        unset($endpoints['/acf/v3/users']);
    }

    if (isset($endpoints['/acf/v3/users/(?P<id>[\\d]+)/?(?P<field>[\\w\\-\\_]+)?'])) {
        unset($endpoints['/acf/v3/users/(?P<id>[\\d]+)/?(?P<field>[\\w\\-\\_]+)?']);
    }


    // DISABLE BLOCK RENDERER
    // Block Renderer is used in classic WP Themes, not in this setup. Therefore it is disabled.


    if (isset($endpoints['/wp/v2/block-renderer/'])) {
        unset($endpoints['/wp/v2/block-renderer/']);
    }

    if (isset($endpoints['/wp/v2/block-renderer/(?P<name>core/block)'])) {
        unset($endpoints['/wp/v2/block-renderer/(?P<name>core/block)']);
    }

    if (isset($endpoints['/wp/v2/block-renderer/(?P<name>core/latest-comments)'])) {
        unset($endpoints['/wp/v2/block-renderer/(?P<name>core/latest-comments)']);
    }

    if (isset($endpoints['/wp/v2/block-renderer/(?P<name>core/archives)'])) {
        unset($endpoints['/wp/v2/block-renderer/(?P<name>core/archives)']);
    }

    if (isset($endpoints['/wp/v2/block-renderer/(?P<name>core/calendar)'])) {
        unset($endpoints['/wp/v2/block-renderer/(?P<name>core/calendar)']);
    }

    if (isset($endpoints['/wp/v2/block-renderer/(?P<name>core/categories)'])) {
        unset($endpoints['/wp/v2/block-renderer/(?P<name>core/categories)']);
    }

    if (isset($endpoints['/wp/v2/block-renderer/(?P<name>core/latest-posts)'])) {
        unset($endpoints['/wp/v2/block-renderer/(?P<name>core/latest-posts)']);
    }

    if (isset($endpoints['/wp/v2/block-renderer/(?P<name>core/rss)'])) {
        unset($endpoints['/wp/v2/block-renderer/(?P<name>core/rss)']);
    }

    if (isset($endpoints['/wp/v2/block-renderer/(?P<name>core/search)'])) {
        unset($endpoints['/wp/v2/block-renderer/(?P<name>core/search)']);
    }

    if (isset($endpoints['/wp/v2/block-renderer/(?P<name>core/shortcode)'])) {
        unset($endpoints['/wp/v2/block-renderer/(?P<name>core/shortcode)']);
    }

    if (isset($endpoints['/wp/v2/block-renderer/(?P<name>core/tag-cloud)'])) {
        unset($endpoints['/wp/v2/block-renderer/(?P<name>core/tag-cloud)']);
    }


    // DISABLE THEMES LISTING
    // This WP setup does not use a 'real' theme, neither is it neccesary to list available
    // themes.

    if (isset($endpoints['/wp/v2/themes'])) {
        unset($endpoints['/wp/v2/themes']);
    }

    // DISABLE BLOCKS
    // TODO: Check if used by Gutenberg. For now, Gutenberg isn't used -> Leave disabled.

    if (isset($endpoints['/wp/v2/blocks'])) {
        unset($endpoints['/wp/v2/blocks']);
    }

    if (isset($endpoints['/wp/v2/blocks/(?P<id>[\\d]+)'])) {
        unset($endpoints['/wp/v2/blocks/(?P<id>[\\d]+)']);
    }

    if (isset($endpoints['/wp/v2/blocks/(?P<id>[\\d]+)/autosaves'])) {
        unset($endpoints['/wp/v2/blocks/(?P<id>[\\d]+)/autosaves']);
    }

    if (isset($endpoints['/wp/v2/blocks/(?P<parent>[\\d]+)/autosaves/(?P<id>[\\d]+)'])) {
        unset($endpoints['/wp/v2/blocks/(?P<parent>[\\d]+)/autosaves/(?P<id>[\\d]+)']);
    }

    return $endpoints;
});
