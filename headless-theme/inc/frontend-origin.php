<?php
/**
 * Frontend origin helper function.
 *
 */

/**
 * Placeholder function for determining the frontend origin.
 * Note: Used for preview of Content from backend and generation of CORS headers.
 *
 * @TODO Determine the headless client's URL based on the current environment.
 *
 * @return str Frontend origin URL, i.e., http://localhost:3000.
 */
function get_frontend_origin() {
    // In case of running your FRONTEND on a local machine, uncomment this.
	return 'http://localhost:3000';
	// In case of running on a remote server, enter the server tld/ path to your FRONTEND APP and uncomment the following return.
    // return 'http://REMOTE.TLD:PORT';

}
