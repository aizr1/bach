# "Headless" WP Theme

## Purpose

Spoofing WP into thinking that there is a proper frontend. Create Functionality for headless setup.

## Features

- Define ACF Routes
- Ensure correct redirects (/wp-json)
- Output only application/json
