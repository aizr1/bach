<?php
// Frontend origin.
// Frontend redirects to RestAPI Endpoint /wp-json, Preview Handling,...
require_once 'inc/frontend-origin.php';

// ACF SYNC for WP-CLI
// Sync ACF Fields from a *.json to db.
// https://support.advancedcustomfields.com/forums/topic/automatic-synchronized-json/#post-41806
require_once 'inc/class-acf-commands.php';

// CORS HANDLING
// Allow requests from every origin
require_once 'inc/cors.php';

// Admin modifications.
require_once 'inc/admin.php';

// Change position of acf fields in classic editor
// Easier usage, prevent used to enter rich content in classic editor
require_once 'inc/acf-position.php';

// Add Headless Settings area.
// require_once 'inc/acf-options.php';

// Add ACF Fields to RestAPI Output for Post Object
// TODO: USE PLUGIN WITH SIMILAR FUNCTION?
// require_once 'inc/acf.php';

// Disable specific routes for security
require_once 'inc/disabled_routes.php';

// Disable comments completely
require_once 'inc/disable-comments.php';

// Remove PHP Version from headers
header_remove("X-Powered-By");
header_remove("Server");

// Disable RSS completely
require_once 'inc/disable-rss-feed.php';

// surface Gutenberg Blocks to REST-API
require_once 'inc/gutenberg-blocks-to-rest.php';
