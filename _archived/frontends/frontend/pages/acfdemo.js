import React, {Component} from 'react';
import Link from 'next/link';
import Router from 'next/router';
import WPAPI from 'wpapi';
import Layout from '../components/Layout';
import Config from '../config';
import axios from 'axios';

const wp = new WPAPI({endpoint: Config.apiUrl});

const tokenExpired = () => {
    if (process.browser) {
        localStorage.removeItem(Config.AUTH_TOKEN);
    }
    wp.setHeaders('Authorization', '');
    Router.push('/login');
};

const baseUrl = "http://localhost/wp-json/acf/v3";

class ACFDemo extends Component {
    state = {
        id: '',
    };

    static async getInitialProps() {
        try {
            const res = await axios.get(`${baseUrl}/posts`);
            const posts = await res.data;
            console.log(`data fetched. Count: ${posts.length}`);
            console.log(posts);

            const res_pages = await axios.get(`${baseUrl}/pages`);
            const pages = await res_pages.data;
            console.log(`data fetched. Count: ${pages.length}`);
            console.log(pages);

            return {posts, pages}
        } catch (err) {
            console.log(err)
        }
        return null;
    }


    render() {
        const {posts, pages} = this.props;

        const acfposts = posts.map(post => {
            const {acf} = post;
            return (
                <li>
                    <Link
                        as={`/post/${post.id}`}
                        href={`/post?slug=${post.id}&apiRoute=post`}
                    >
                        <a>{acf.title} - {acf.text} - [ {acf.image_title} ]</a>
                    </Link>
                </li>
            );
        });

        const acfpages = pages.map(page => {
            const {acf} = page;
            return (
                <li>
                    <Link
                        as={`/post/${page.id}`}
                        href={`/post?slug=${page.id}&apiRoute=post`}
                    >
                        <a>{acf.title} - {acf.text}</a>
                    </Link>
                </li>
            );
        });

        return (
            <Layout>
                <h1>ACF Posts</h1>
                <ul>
                    {acfposts ? acfposts : null}
                </ul>
                <h1>ACF Pages</h1>
                <ul>
                    {acfpages ? acfpages : <li>No pages to display.</li>}
                </ul>
            </Layout>
        );
    }
}

export default ACFDemo;
