import React, { Component } from 'react';
import Link from 'next/link';
import Router from 'next/router';
import WPAPI from 'wpapi';
import Layout from '../components/Layout';
import PageWrapper from '../components/PageWrapper';
import Menu from '../components/Menu';
import Config from '../config';
import Error from "next/error";

const wp = new WPAPI({ endpoint: Config.apiUrl });

const headerImageStyle = {
    marginTop: 50,
    marginBottom: 50,
};

const tokenExpired = () => {
    if (process.browser) {
        localStorage.removeItem(Config.AUTH_TOKEN);
    }
    wp.setHeaders('Authorization', '');
    Router.push('/login');
};

class Index extends Component {
    state = {
        id: '',
    };

    static async getInitialProps() {
        try {
            const [page, posts, pages] = await Promise.all([
                wp
                    .pages()
                    .slug('welcome')
                    .embed()
                    .then(data => {
                        return data[0];
                    }),
                wp.posts().embed(),
                wp.pages().embed(),
            ]);

            return { page, posts, pages };
        } catch (err) {
        }
        return null;
    }

    componentDidMount() {
        const token = localStorage.getItem(Config.AUTH_TOKEN);
        if (token) {
            wp.setHeaders('Authorization', `Bearer ${token}`);
            wp.users()
                .me()
                .then(data => {
                    const { id } = data;
                    this.setState({ id });
                })
                .catch(err => {
                    console.error(err)
                    // if (err.data.status === 403) {
                    //     tokenExpired();
                    // }
                });
        }
    }

    render() {
        const { id } = this.state;
        const { posts, pages, headerMenu, page } = this.props;

        if (!posts) return <Error statusCode={404} />;

        const fposts = posts.map(post => {
            return (
                <div key={post.slug}>
                        <Link
                            as={`/post/${post.slug}`}
                            href={`/post?slug=${post.slug}&apiRoute=post`}
                        >
                            <h1><em>WPPost Title:</em> {post.title.rendered}</h1>
                        </Link>
                        <h2>Post Text</h2>
                        <div
                            // eslint-disable-next-line react/no-danger
                            dangerouslySetInnerHTML={{
                                __html: post.content.rendered,
                            }}
                        />
                        <h2>ACF Fields</h2>
                        <ul>
                            <li>Simple Title: {post.acf.title}</li>
                            <li>Simple Text {post.acf.text}</li>
                            <li>Simple Image {post.acf.image_text}</li>
                            <li><img width='200' src={post.acf.image_file} alt=""/></li>
                            <li>Testimonial Author: {post.acf.author}</li>
                            <li>Testimonial Avatar: {post.acf.avatar}</li>
                            <li>Testimonial Quote: {post.acf.testimonial}</li>
                        </ul>
                    <hr/>
                    <div style={{ "height": 50 }}/>
                </div>
            );
        });
        const fpages = pages.map(ipage => {
            return (
                <ul key={ipage.slug}>
                    <li>
                        <Link
                            as={`/page/${ipage.slug}`}
                            href={`/post?slug=${ipage.slug}&apiRoute=page`}
                        >
                            <a>{ipage.title.rendered}</a>
                        </Link>
                    </li>
                </ul>
            );
        });
        return (
            <Layout>
                <h1>{page.title.rendered}</h1>
                <div
                    // eslint-disable-next-line react/no-danger
                    dangerouslySetInnerHTML={{
                        __html: page.content.rendered,
                    }}
                />
                <h2>Posts</h2>
                {fposts ? fposts : <h4>We're sorry. There was a small problem with the server.w</h4>}
                <h2>Pages</h2>
                {fpages}
                {id ? (
                    <div>
                        <h2>You Are Logged In</h2>
                        <p>
                            Your user ID is <span>{id}</span>, retrieved via an authenticated
                            API query.
                        </p>
                    </div>
                ) : (
                    <div>
                        <h2>You Are Not Logged In</h2>
                        <p>
                            The frontend is not making authenticated API requests.{' '}
                            <a href="/login">Log in.</a>
                        </p>
                    </div>
                )}
            </Layout>
        );
    }
}

export default PageWrapper(Index);
