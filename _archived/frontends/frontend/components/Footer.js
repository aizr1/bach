import React from 'react';

const hrStyle = {
  marginTop: 75,
};

const Footer = () => (
  <div>
    <hr style={hrStyle} />
    <p>A Footer Base</p>
  </div>
);

export default Footer;
