import React from 'react';
import Head from 'next/head';

const Header = () => (
  <div>
    <Head>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta charSet="utf-8" />
      <title>wp next_js</title>
    </Head>
  </div>
);

export default Header;
