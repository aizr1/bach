#!/bin/sh
for ((I=1; I<=50; I++))
do
convert -size 1280x720 plasma:fractal -draw "scale 4,4 gravity center text 0,0 '$I'" -quality 70 backup/img_$I.jpg
done
exit;
